[![pipeline status](https://gitlab.com/StarDancer/example-cicd/badges/main/pipeline.svg)](https://gitlab.com/StarDancer/example-cicd/-/commits/main)
[![coverage report](https://gitlab.com/StarDancer/example-cicd/badges/main/coverage.svg)](https://gitlab.com/StarDancer/example-cicd/-/commits/main)
[![Latest Release](https://gitlab.com/StarDancer/example-cicd/-/badges/release.svg)](https://gitlab.com/StarDancer/example-cicd/-/releases)


```mermaid 
%%{init: { 'theme':'dark', 'sequence': {'useMaxWidth':false} } }%%
sequenceDiagram 
alice ->> mark: Sent a flower
```
