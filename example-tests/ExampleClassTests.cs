using example_api;

namespace example_tests;

public class ExampleClassTests
{
    [Fact]
    public void ReturnStringFromMethod1()
    {
        ExampleClass exampleClass = new ExampleClass();

        string result = exampleClass.Method1();
        
        Assert.Equal("Some string", result);
    }

    [Theory]
    [InlineData(5, 5)]
    [InlineData(12, 12)]
    public void ReturnEvenNumberFromMethod2(int a, int b)
    {
        ExampleClass exampleClass = new ExampleClass();

        bool result = exampleClass.Method2(a, b) % 2 == 0;
        
        Assert.True(result);
    }
    
    [Theory]
    [InlineData(5, 6)]
    [InlineData(12, 13)]
    public void ReturnOddNumberFromMethod2(int a, int b)
    {
        ExampleClass exampleClass = new ExampleClass();

        bool result = exampleClass.Method2(a, b) % 2 == 1;
        
        Assert.True(result);
    }
}