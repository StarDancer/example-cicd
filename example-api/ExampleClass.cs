namespace example_api;

public class ExampleClass
{
    public string Method1()
    {
        return "Some string";
    }

    public int Method2(int a, int b)
    {
        return a + b;
    }
}